package practica;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class App {
	
	public static void main(String[] args) {
		
		Conector co = Conector.getConector();
		
		List<Device> dispositivos = new ArrayList<>();
		dispositivos = co.consulta("SELECT * FROM DEVICE;");
		
		for(Device c : dispositivos) {
        	System.out.println(c.toString());
        }
		
		System.out.println("Interfaces-------------");
		
		List<String> listaStrings = dispositivos.stream()
						.map(Device::getName)
						.filter(c -> c.equals("Laptop"))
						.collect(
									Collectors.toList()
								);
		listaStrings.forEach(System.out::println);
        
		int totalDisp = (int) dispositivos.stream()
						.filter((c) -> c.getManufacturerId() == 3)
						.count();
		System.out.println("Total: " + totalDisp);
		
		List<Device> lista2 = dispositivos.stream()
								.filter((disp) -> disp.getColorId() == 1)
								.collect(
										Collectors.toList()
										);
		lista2.forEach(System.out::println);
		
		
		Map<Integer,Device> mapa = dispositivos.stream()
				.collect(
						Collectors.toMap(Device::getDeviceId, Device::getInstance)
						);
		mapa.forEach((k,v) -> System.out.println(k + " " + v.toString()));
						
						
						
    }
}


