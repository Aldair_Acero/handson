package practica;

import java.util.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public final class Conector implements ConexionMysql {
	
	private static final Conector conexion = new Conector();
	
	private Conector() {}
	
	public static Conector getConector(){
		return conexion;
	}

	
	
	public List<Device> consulta(String query){
		
		List<Device> dispositivos = new ArrayList<>();
		
		
			try{Connection conn = this.conexionI();
            PreparedStatement preparedStatement = conn.prepareStatement(query);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
               /* long id = resultSet.getLong("COLORID");
                String name = resultSet.getString("NAME");
                String hexValue = resultSet.getString("HEXVALUE");*/
            	int deviceId = resultSet.getInt("DEVICEID");
            	String name = resultSet.getString("NAME");
            	String description = resultSet.getString("DESCRIPTION");
            	int manufacturerId = resultSet.getInt("MANUFACTURERID");
            	int colorId = resultSet.getInt("COLORID");
            	dispositivos.add(new Device(deviceId,name,description,manufacturerId,colorId));
            }
			}catch(Exception e) {
				System.out.println("Error en la conexion 2");
			}
                
        
		
		return dispositivos;
		
	}

	@Override
	public Connection conexionI() {
		System.out.println("MySQL JDBC Connection");
		Connection conn = null;
		try {
			conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/sesion3?serverTimezone=UTC#", "root", "1234");
		} catch (SQLException e) {
			System.out.println("Error en la conexion 1");
			e.printStackTrace();
		}
		return conn;
	}


	
}


