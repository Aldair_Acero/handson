package practica;

import java.sql.Connection;

@FunctionalInterface
public interface ConexionMysql {
	public Connection conexionI();
}
